#!/usr/bin/env python

"""send current time in RTC format to client running Firmata"""

import argparse
import time

from pyfirmata import Arduino, util


def mkbcd(val):
    return '{:02d}'.format(val)


def send(msg):
    client.send_sysex(0x71, util.str_to_two_byte_iter(msg))
    print(msg)


parser = argparse.ArgumentParser(description="get current time and send it to Firmata client")

parser.add_argument('-p', '--port', action='store', required=True)
parser.add_argument('-s', '--speed', action='store', default='115200')

args = parser.parse_args()


client = Arduino(args.port, baudrate=int(args.speed))

time.sleep(1)

send("synch 1")

time.sleep(1)

send("synch 2")

time.sleep(1)

now = time.localtime(time.time())

out = ''

out += mkbcd(now.tm_year % 100)

out += mkbcd(now.tm_mon)
out += mkbcd(now.tm_mday)
out += mkbcd(now.tm_wday)
out += mkbcd(now.tm_hour)
out += mkbcd(now.tm_min)
out += mkbcd(now.tm_sec)

send(out)
