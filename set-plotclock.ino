#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Firmata.h>

#define LED     13
#define ADDR_LCD 0x27
#define ADDR_RTC 0x68

// SDA A4 SCL A5 for Uno

// I2C addr, RS, RW, EN, D4, D5, D6, D7, backlight, backlight polarity
static LiquidCrystal_I2C lcd(ADDR_LCD, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
static long next;
static char buf[3];

static byte
getdigits(
  char  * ptr)
{
  return ((ptr[0] - '0') << 4) | (ptr[1] - '0');
}

static void
lcdDisplayString(
  char *  msg)
{
  char * ptr;

  lcd.setCursor(0,1);
  lcd.print(msg);

  for (ptr = msg; *ptr != '\0'; ++ptr) {
    if ((*ptr < '0') || (*ptr > '9')) {
      return;
    }
  }

  if ((ptr - msg) != 14) {
    return;
  }

  Wire.beginTransmission(ADDR_RTC);
  Wire.write(0);
  Wire.write(getdigits(msg + 12));  // second
  Wire.write(getdigits(msg + 10));  // minute
  Wire.write(getdigits(msg +  8));  // hour
  Wire.write(getdigits(msg +  6));  // day of week
  Wire.write(getdigits(msg +  4));  // day of month
  Wire.write(getdigits(msg +  2));  // month
  Wire.write(getdigits(msg +  0));  // year (2 digit)
  Wire.write(0);
  Wire.endTransmission();
}

static void
lcdprint(
  byte val)
{
  sprintf(buf, "%02x", val);
  lcd.print(buf);
}

void
setup()
{  
  //Firmata.setFirmwareVersion(0, 1);
  Firmata.attach(STRING_DATA, lcdDisplayString);
  Firmata.begin(115200);

  //Wire.begin();

  lcd.begin(16,2);
  lcd.home();
  lcd.print("Hello, ARDUINO ");

  pinMode(LED, OUTPUT);

  next = 0;
}

void
loop()
{
  byte seconds;
  byte minutes;
  byte hours;
  byte dayweek;
  byte monthday;
  byte months;
  byte years;
  byte temp;
  long cur;

  while (Firmata.available()) {
    Firmata.processInput();
  }

  cur = millis();

  if (cur > next) {
    Wire.beginTransmission(ADDR_RTC);
    Wire.write(0);
    Wire.endTransmission();
    Wire.requestFrom(ADDR_RTC, 7);
    seconds  = Wire.read();
    minutes  = Wire.read();
    hours    = Wire.read();
    dayweek  = Wire.read();
    monthday = Wire.read();
    months   = Wire.read();
    years    = Wire.read();

    Wire.beginTransmission(ADDR_RTC);
    Wire.write(0x11);
    Wire.endTransmission();
    Wire.requestFrom(ADDR_RTC, 1);
    temp = Wire.read();

    lcd.setCursor(0,0);
    lcdprint(years);
    lcdprint(months);
    lcdprint(monthday);
    lcdprint(dayweek);
    lcdprint(hours);
    lcdprint(minutes);
    lcdprint(seconds);

    sprintf(buf, "%2d", temp);
    lcd.print(buf);
    
    next = cur + 1000;
  }
}


